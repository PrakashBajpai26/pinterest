var app = angular.module('httpApp', []);

app.controller('httpAppCtrlr', function ($scope, $http) {
    $http.get('http://api.giphy.com/v1/gifs/search?q=trending&api_key=dc6zaTOxFJmzC')
        .then(function (response) {
        $scope.images = response.data.data;
    });
});